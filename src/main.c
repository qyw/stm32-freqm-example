/**
 * Copyright (c) 2016 [Ivan Kuvaldin](mailto:i.kyb@ya.ru)
 * [The MIT License](https://opensource.org/licenses/MIT)
 */

#include "stm32-freqm/freqM.h"

#include "Sledge/bsp.h"
#define DEBUG_LEVEL 7
#include "Sledge/debug.h"
#include "Sledge/assert.h"


///\note В ПРЕРЫВАНИИ.
static void freqm_ready_cb(float freq, void *par)
{
	bsp_LedToggle(LED_Blue);
	bsp_LedOff(LED_Red);
	printf("freq=%f\n",freq);
}

///\note В ПРЕРЫВАНИИ.
static void freqm_fail_cb(void *par)
{
	bsp_LedOn(LED_Red); bsp_LedOff(LED_Blue);
	printf("cannot count frequency.\n");
}


/** 
 * main bootloader function
 */
int main()
{
	//DBGMCU_APB1PeriphConfig( DBGMCU_WWDG_STOP | DBGMCU_IWDG_STOP, ENABLE );
	//SystemInit();
	//Sledge_init();
	fopen("/dev/uart0","");
	bsp_LedInitAll();
	printf("Hello from stm32-freqM-example\n"
	       "Visit https://bitbucket.org/qyw/stm32-freqm-example.git for more information.\n"
	       "=====================================================================\n");
	
	freqM_Settings_t s = freqM_Settings_DEFAULTS;
	freqM_Callback_t cb = {freqm_ready_cb,NULL};
	s.callback = cb;
	freqM_FailCallback_t fcb = {freqm_fail_cb,NULL}; //fcb.func = freqm_fail_cb; cb.params=NULL;
	s.fail_callback = fcb;
	freqM_init(&s);
	
	bsp_LedOn(LED_Red);
	while(1){
		int delay = 40000000;
		bsp_LedToggle(LED_Green);
		while(delay-- > 0);
	}
}


