/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php  
 *      http://opensource.org/licenses/mit-license.php  
 */
/**
 * @file    Sledge/c11n/c11n.c
 * @author  Kyb Sledgehammer <i.kyb[2]ya,ru>
 * @brief 	Look at ./c11n.h for more information.
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "Sledge/utils.h"
#include "Sledge/c11n/c11n.h"
#include "Sledge/assert.h"

#include "stm32-freqm/freqM.h"
#include "stm32-freqm/freqM_config.h"

#include "./device.h"



int volatile testParam = 125;

extern /*float*/void* vibroNormalizeCoefB_get(void);
extern void vibroNormalizeCoefB_set(float);

//extern int  vibroNormalizeCoefK_get(void);
//extern void vibroNormalizeCoefK_set(int);
extern float vibroNormalizeCoefK_get(void);
extern void vibroNormalizeCoefK_set(float);

extern void vibroRecalculateToDeviation_set( bool value );
extern bool vibroRecalculateToDeviation_get( void );

extern void vibroUseArtificialSinus_set( bool value );
extern bool vibroUseArtificialSinus_get( void );

extern Parameter_t vibro_PARAMETERS[];
extern const size_t vibro_PARAMETERS_COUNT;

static GETTER_FUNCTION( testParam_get_ );
static SETTER_FUNCTION( testParam_set_ ); 	//static SETTER_FUNCTION( testParam_set_, pValue );


extern const Parameter_t indication_PARAMETERS[];
extern const size_t indication_PARAMETERS_COUNT;


/// Must be sorted in ASCENDING order
const Parameter_t PARAMETERS[] = {
//	{"hwInputFilter"              ,             NULL,  C11N_UINT16, (setterFunction_t)              hwInputFilter_set, (getterFunction_t)              hwInputFilter_get, 0,0,    defaultHW_INPUT_FILTER, "Hardware input filter" },
//	{"hwInputPrescaler"           ,             NULL,  C11N_UINT16, (setterFunction_t)           hwInputPrescaler_set, (getterFunction_t)           hwInputPrescaler_get, 0,0, defaultHW_INPUT_PRESCALER, "" },
//	{"sampleRateInput"            ,             NULL,  C11N_UINT16, (setterFunction_t)            sampleRateInput_set, (getterFunction_t)            sampleRateInput_get, 0,0,        defaultSAMPLE_RATE, "" },
	//{"setpointAlarm"              ,             NULL,   C11N_FLOAT, (setterFunction_t)    vibroUseArtificialSinus_set, (getterFunction_t)    vibroUseArtificialSinus_get, 0,0,                      3300, "" },
	//{"setpointWarnHi"             ,             NULL,   C11N_FLOAT, (setterFunction_t)    vibroUseArtificialSinus_set, (getterFunction_t)    vibroUseArtificialSinus_get, 0,0,                      3030, "" },
	//{"setpointWarnLow"            ,             NULL,   C11N_FLOAT, (setterFunction_t)    vibroUseArtificialSinus_set, (getterFunction_t)    vibroUseArtificialSinus_get, 0,0,                      2970, "" },
	{"testParam"                  ,(void*)&testParam,   C11N_INT32, (setterFunction_t)                 testParam_set_,                                    testParam_get_, 0,0,                       125, "" },
//	{"timfreqbase"                ,             NULL,  C11N_UINT32, (setterFunction_t)                timfreqbase_set, (getterFunction_t)                timfreqbase_get, 0,0,  defaultTIM_REF_BASE_FREQ, "" },
//	{"vibroNormalizeCoefB"        ,             NULL,   C11N_FLOAT, (setterFunction_t)        vibroNormalizeCoefB_set, (getterFunction_t)        vibroNormalizeCoefB_get, 0,0,                         0, "" },
//	{"vibroNormalizeCoefK"        ,             NULL,   C11N_FLOAT, (setterFunction_t)        vibroNormalizeCoefK_set, (getterFunction_t)        vibroNormalizeCoefK_get, 0,0,                         1, "" },
//	{"vibroRecalculateToDeviation",             NULL, C11N_BOOLEAN, (setterFunction_t)vibroRecalculateToDeviation_set, (getterFunction_t)vibroRecalculateToDeviation_get, 0,0,                     false, "" },
//	{"vibroUseArtificialSinus"    ,             NULL, C11N_BOOLEAN, (setterFunction_t)    vibroUseArtificialSinus_set, (getterFunction_t)    vibroUseArtificialSinus_get, 0,0,                     false, "" },
	//{ "multiSender.autostart"                                           },
	//{ "multiSender.ip"                                                  },
	//{ "multiSender.port"                                                },
};
const size_t PARAMETERS_COUNT = sizeofarr(PARAMETERS);

const ModulesParameters_t general_CONFIGURATION = {
	"", PARAMETERS, sizeofarr(PARAMETERS)
};


//#include "sensor_self_test.h"
extern const ModulesParameters_t 
	if5646_CONFIGURATION,
	indication_CONFIGURATION,
	sensor_test_CONFIGURATION,
	vibro_CONFIGURATION;

const ModulesParameters_t * MODULES_PARAMETERS[] = {
	&general_CONFIGURATION,
//	&if5646_CONFIGURATION,
	&indication_CONFIGURATION,
	&sensor_test_CONFIGURATION,
	&vibro_CONFIGURATION,
	//device_CONFIGURATION
	//freqm
	//general
	//multiSender
	//sd
	//udpsender
};
const size_t MODULES_PARAMETERS_COUNT = sizeofarr(MODULES_PARAMETERS);


static GETTER_FUNCTION( testParam_get_ )
{
	c11n_Type ret;
	ret.int32 = testParam;
	return ret;

	//return testParam;
	//return (void*)testParam;
}

static SETTER_FUNCTION( testParam_set_ )//, pValue )
{
	//testParam = pValue;
	testParam = value.int32;//(int)pValue;
}
