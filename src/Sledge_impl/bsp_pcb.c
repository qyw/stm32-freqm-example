/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php <br>
 *      http://opensource.org/licenses/mit-license.php <br>
 */
/**
 * \file 	Sledge/bsp/bsp_pcb.c
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.Kyb[2]ya,ru>
 * \version	2.6.0
 * \date 	2015-Apr-30
 * \brief 	This sub-module is a part of Sledge BSP module.
 * 			This file is a skeleton where must be defined GPIO purposes for concrete board
 * 			Board name should be defined globally in project configuration.
 */

#include "bsp_pcb.h"
#include "Sledge/bsp.h"
#include "Sledge/utils.h"

#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_dma.h"


/** ┌———————————————————————————————————————————————————————————————————————┐
	│						Outouts & LEDs control							│  
	└———————————————————————————————————————————————————————————————————————┘ */
#ifdef _LEDS_
 /// List of regular LEDs
 const GPIO_t* LEDS[] = {_LEDS_};
 const uint8_t numOfLEDS = sizeofarr(LEDS);
#else
 const uint8_t numOfLEDS = 0;
#endif

/// List of LEDs with controllable brightness.
#ifdef _LEDS_PWM_
 const GPIO_t* LEDS_PWM[] = {_LEDS_PWM_};  //pcbLEDS_PWM;
 const uint8_t numOfLEDS_PWM = sizeofarr(LEDS_PWM);
#else
 const uint8_t numOfLEDS_PWM = 0;
#endif

/// List of RGB LEDs with controlable color and brightness
#ifdef _LEDS_RGB_
 LedRGB_t *const LEDS_RGB[] = 	{ &_LEDS_RGB[0] 
								};
 /*const*/ LedRGB_t _LEDS_RGB[] = _LEDS_RGB_;
 const uint8_t numOfLEDS_RGB = sizeofarr(LEDS_RGB);
#else
 LedRGB_t *const LEDS_RGB[] = {0};
 const uint8_t numOfLEDS_RGB = 0;
#endif


/** ┌———————————————————————————————————————————————————————————————————————┐
	│					Segment indicators GPIOs							│  
	└———————————————————————————————————————————————————————————————————————┘ */
#ifdef _CommonGpios_
 const GPIO_t* CommonGpios[] = _CommonGpios_;
 const uint8_t numOfSiCommonGpios = sizeofarr(CommonGpios);
#else
 const uint8_t numOfSiCommonGpios = 0;
#endif

#ifdef _SegmentGpios_
 const GPIO_t* SegmentGpios[] = _SegmentGpios_;
 const uint8_t numOfSiSegmentGpios = sizeofarr(SegmentGpios);
#else
 const uint8_t numOfSiSegmentGpios = 0;
#endif


/** ┌———————————————————————————————————————————————————————————————————————┐
	│				Inputs, Buttons and joystick control					│  
	└———————————————————————————————————————————————————————————————————————┘ */
#ifdef _BUTTONS_
 const GPIO_t* BUTTONS[] = _BUTTONS_;  //pcbBUTTONS;
 const uint8_t numOfButtons = sizeofarr(BUTTONS);
#else
 const uint8_t numOfButtons = 0;
#endif


/** ┌———————————————————————————————————————————————————————————————————————┐
	│							COM ports									│  
	└———————————————————————————————————————————————————————————————————————┘ */
#ifdef _COMS_
 const COMPort_t COMS[] = _COMS_;
 const uint8_t numOfCOMs = sizeofarr(COMS);
#else
 const uint8_t numOfCOMs = 0;
#endif
const COMPort_t COM_debug = { _COM_debug_ };
