/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family.
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */
/**
 * \file 	Sledge/bsp/bsp_pcb.h
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.Kyb[2]ya,ru>
 * \version	2.6.0
 * \date 	2015-Apr-30
 * \brief 	This sub-module is a part of Sledge BSP module.
 * 			This code is to select proper file to include in accordance to defined board.
 * 			The PCB should be defined globally in project.
 */

#ifndef __bsp_pcb_H_
#define __bsp_pcb_H_ 260


#if defined(PCB_STM32F4DISCOVERY)
 #define PCB_NAME  "STM32F4Discovery"
 #include "./PCBs/pcb_STM32F4-Disco.h"

/*#elif defined( PCB_MY )
 #define PCB_NAME  "My"
 #include "./PCBs/pcb_my.h"*/

#else
 #error Unknown PCB or PCB is not defined.
#endif


#endif //__bsp_pcb_H_
