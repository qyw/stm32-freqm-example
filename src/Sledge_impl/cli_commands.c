/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php  
 *      http://opensource.org/licenses/mit-license.php  
 */
/**
 * \file 	Sledge/cli/cli-commands.c
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * \brief 	This file is part of Sledge/cli module. For more information look at ./cli.h
 * 
 * This file is user-defined and must 
 * provide commands list and functions implementation.
 */

#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
//#include <ctype.h>
//#include <stdio.h>

#include "FreeRTOS.h"
#include "task.h"

#include "lwip/netif.h"

#include "network/udpSenderOptions.h"
//#include "network/udpSender.h"
#include "network/vmp_network.h"
#include "ptpd-2.0.0/src/ptpd.h"
#include "network/vmp_archive.h"

//#include "mainConfig.h"
#undef DEBUG_LEVEL
#define DEBUG_LEVEL  CLI_DEBUG_LEVEL
#include "Sledge.h"
/*#include "Sledge/cli/cli.h"
#include "Sledge/c11n/c11n.h"
#include "Sledge/utils.h"
#include "Sledge/hal/uid.h"
//#include "Sledge/strtrim.h"*/
//#include "Sledge/drv/stm32/sledge_stm32_RTC.h"


/**
 * All following functions have the same syntacsis.
 * @param[OUT] *response 	the pointer to string which will contain response
 * @param[IN]   respLen		max length of response
 * @param[IN]   argc 		arguments number
 * @param[IN]  *argv[] 		arguments
 * @param[IN]  *opts 		Options which will be passed to concrete command processor
 * @retval Error code. 0 is OK. 1 is signal to close connection. <0 - any error
 */
static cli_COMMAND_FUNCTION( cli_udpSenderStart );
static cli_COMMAND_FUNCTION( cli_udpSenderStop );

static cli_COMMAND_FUNCTION( cli_get  );
static cli_COMMAND_FUNCTION( cli_set  );
static cli_COMMAND_FUNCTION( cli_param );

static cli_COMMAND_FUNCTION( cli_time );
static cli_COMMAND_FUNCTION( cli_date );
static cli_COMMAND_FUNCTION( cli_dateptp );
static cli_COMMAND_FUNCTION( cli_ptpd2 );

static cli_COMMAND_FUNCTION( cli_help );
static cli_COMMAND_FUNCTION( cli_info );
static cli_COMMAND_FUNCTION( cli_tasklist );
static cli_COMMAND_FUNCTION( cli_clear );
static cli_COMMAND_FUNCTION( cli_exit );
//static cli_COMMAND_FUNCTION( cli_unknown );
//static cli_COMMAND_FUNCTION( cli_reset );

static cli_COMMAND_FUNCTION( cli_archive );


/// MUST be sorted in ASCENDING order.
const CliCommand_t COMMANDS[] = {
	{"?"	, 		      cli_help, "The same as help2" },
	{"archive",        cli_archive, "Read archive from SD" },
	{"clear", 		     cli_clear, "Clear the screen" },
	{"cls"  , 		     cli_clear, "The same as clear" },
	{"date" , 		      cli_date, "Show/set current date and time with RTC" },
	{"dateptp",        cli_dateptp, "Show current date and time by PTP" },
	{"datertc", 	      cli_date, "Show current date and time by RTC" },
	{"exit" , 		      cli_exit, "Close telnet connection" },
	{"get"  , 		       cli_get, "Get variable/parameter" },
	{"hello", 		      cli_info, "The same as info" },
	{"HELP" , 		      cli_help, "Show this short help" },
	{"info", 		      cli_info, "Print information about device and firmware" },
	{"param",            cli_param, "Universal get/set command" },
	{"ptpd" , 		     cli_ptpd2, "Show PTPd2 stats. TODO" },
	{"ptpd2" , 		     cli_ptpd2, "Show PTPd2 stats. TODO" },
	{"reset",            cli_reset, "Reset device" },
	{"sd"  , 	              NULL, "Command for SD card. Info and settings. TODO" },
	{"set"  , 		       cli_set, "Set variable/parameter" },
	{"show" , 		       cli_get, "The same as get" },
	{"start", 	cli_udpSenderStart, "Start udpSender to your IP" },
	{"stats", 	              NULL, "Statistics" },
	{"stop" , 	 cli_udpSenderStop, "Stop udpSender" },
	{"time",              cli_time, "date command alias" },
	{"tasklist",      cli_tasklist, "Show the list of RTOS tasks" },
};

const size_t COMMANDS_COUNT = array_length(COMMANDS);


/* 
 * ==== FUNCTIONS ====
 */

/**
 * Read archive from SD
 */
static cli_COMMAND_FUNCTION( cli_archive )
{
	char *endptr;
	//time_t time_from, time_to;
	ArchiveSenderOptions *aso = malloc(sizeof(ArchiveSenderOptions));
	
	if( argc > 3 ){
		strlcpy( response, "Usage: archive [time_from] [time_to]\n  time in seconds\n"NEWLINE, respLen );
		return 0;
	}
	
	aso->time_from = strtoul( argv[1], &endptr, 10 );
	if( endptr == argv[1] )
		goto wrong_args;
	
	if( argc == 3 ){
		aso->time_to = strtoul( argv[2], &endptr, 10 );
		if( endptr == argv[2] )   // невозможно распознать число
			goto wrong_args;
	}else{ // задан только time_to
		aso->time_to = ULONG_MAX;  // Infinite unsigned
	}
	if( aso->time_from == ULONG_MAX || aso->time_to <= aso->time_from )
		goto wrong_args;
	
	aso->destinationIP = ((SenderOptions_t*)opts)->destinationIP;
	aso->port = 4334;
	
	// Создать задачу, которая отправляет архив на IP адрес 
	xTaskCreate( archiveSender_task, "archiveSender_task", configMINIMAL_STACK_SIZE *3, aso, MAIN_TASK_PRIO, NULL );

	strlcpy( response, "Accepted. Transmission in progress."NEWLINE, respLen );
	return 0;
	
wrong_args:
	strlcpy( response, "Wrong arguments"NEWLINE, respLen );
	return 0;
}


/**
 * Start the udpSender task, if not already started.
 */
cli_COMMAND_FUNCTION( cli_udpSenderStart )
{
	err_t err;
	
	err = vmp_udpSenderStart(opts);	
	switch( err ){
		case ERR_OK:
			//*response = "Already created."NEWLINE;
			strlcpy( response, "Already created."NEWLINE, respLen);
			break;
		case 2:
			strlcpy( response, "Just created."NEWLINE, respLen);
			break;
		case ERR_MEM:
			strlcpy( response, "Error!"NEWLINE, respLen);
			break;
	}

	return err;
}


/**
 * Stop the udpSender task.
 */
cli_COMMAND_FUNCTION( cli_udpSenderStop )
{
	if( vmp_udpSenderTaskStop() ){
		//*response = "Stoped."NEWLINE;	
		strlcpy( response, "Stoped."NEWLINE, respLen);
	} else {
		strlcpy( response, "Already stoped."NEWLINE, respLen);
	}
	return 0;
}


/** 
 * Get the value of parameter.
 */
cli_COMMAND_FUNCTION( cli_get )
{
	const Parameter_t *parameter = NULL;
	float val_f;
	char str_buf[20], *val_str=str_buf;
	
	if( argc != 2 ){
		strlcpy( response, "Usage: get <paramName>"NEWLINE, respLen );
		return 0;
	}
	
	parameter = c11n_findParameterByName( argv[1] );
	
	if( parameter == NULL ){
		snprintf( response, respLen, "Parameter %s not found"NEWLINE, argv[1] );
		return 0;
	}
	
	if( parameter->get == NULL ){
		snprintf( response, respLen, "Parameter %s is write-olny"NEWLINE, parameter->name );
		return 0;
	}
	
	switch( parameter->type ){
		case C11N_INT8:
		case C11N_INT16:
		case C11N_INT32:
			snprintf( response, respLen, "%s = %d"NEWLINE, parameter->name, parameter->get().int32 );
			break;
		
		case C11N_UINT8:
		case C11N_UINT16:
		case C11N_UINT32:
			snprintf( response, respLen, "%s = %u"NEWLINE, parameter->name, parameter->get().uint32 );
			break;
		
		case C11N_FLOAT:
			val_f = parameter->get().floa;
			if( isnan(val_f) )
				val_str = "NaN";
			else if( isinf(val_f) )
				val_str = "Inf";
			else
				snprintf(val_str, sizeof(val_str), "%f", val_f);
				
			snprintf( response, respLen, "%s = %s"NEWLINE, parameter->name, val_str );
			//snprintf( response, respLen, "%s = %f"NEWLINE, parameter->name, parameter->get().floa );
			break;
		
		case C11N_BOOLEAN: 
			snprintf( response, respLen, "%s = %s"NEWLINE, parameter->name, parameter->get().boolean ? "true" : "false" );
			break;
		
		case C11N_INT64:
		case C11N_UINT64:
		case C11N_DOUBLE:
		case C11N_STRUCT:
			break;
	}
	//snprintf( response, respLen, "%s = %d"NEWLINE, param->name, param->get() );
	
	return 0;
}


/** 
 * Set the Parameter with Value
 */
cli_COMMAND_FUNCTION( cli_set )
{
	const Parameter_t *parameter;
	/*union {
		int32_t  int32;
		uint32_t uint32;
		float floa;
		void* pVoid;
	} value;*/
	c11n_Type value;
	
	if( argc != 3 ){
		strlcpy( response, "Usage: set <paramName> <value>"NEWLINE, respLen);
		return 0;
	}
		
	parameter = c11n_findParameterByName( argv[1] );
	debugf3("parameter: %u"NEWLINE, (uint32_t)parameter);
	
	if( parameter == NULL ){
		snprintf( response, respLen, "Parameter %s not found"NEWLINE, argv[1] );
		return 0;
	}
	
	if( parameter->set == NULL ){
		snprintf( response, respLen, "Parameter %s is read-olny"NEWLINE, parameter->name );
	}
	
	switch( parameter->type ){
		case C11N_INT8:
		case C11N_INT16:
		case C11N_INT32:
			value.int32 = strtol( argv[2], NULL, 10 );
			parameter->set( value );
			if( parameter->get ){
				value = parameter->get(); //value.pVoid = parameter->get().pVoid;
				snprintf( response, respLen, "%s = %d"NEWLINE, parameter->name, value.int32 );
			} else {
				strncpy( response, "accepted"NEWLINE, respLen ); //response[0] = '\0';
			}
			break;
		
		case C11N_UINT8:
		case C11N_UINT16:
		case C11N_UINT32:
			///\TODO поддержка 0x
			value.uint32 = strtoul( argv[2], NULL, 10 );
			parameter->set( value );
			if( parameter->get ){
				value = parameter->get(); //value.pVoid = parameter->get().pVoid;
				snprintf( response, respLen, "%s = %u"NEWLINE, parameter->name, value.uint32 /*parameter->get()*/ );
			} else {
				strncpy( response, "accepted"NEWLINE, respLen ); //response[0] = '\0';
			}
			break;
		
		case C11N_FLOAT:
			value.floa = strtof(argv[2], NULL);
			parameter->set( value );
			if( parameter->get ){
				value = parameter->get(); //value.pVoid = parameter->get().pVoid;
				snprintf( response, respLen, "%s = %f"NEWLINE, parameter->name, value.floa );
			} else {
				strncpy( response, "accepted"NEWLINE, respLen ); //response[0] = '\0';
			}
			break;
		
		case C11N_BOOLEAN: 
			if( strcasecmp(argv[2], "true") == 0 ){
				value.boolean = true;
			} else if ( strcasecmp(argv[2], "false") == 0 ){						
				value.boolean = false;
			} else {
				snprintf( response, respLen, "Parameter %s is boolean. Use true or false"NEWLINE, parameter->name );
				break;
			}
			parameter->set( value );
			if( parameter->get )
				snprintf( response, respLen, "%s = %s"NEWLINE, parameter->name, parameter->get().int8 ? "true" : "false" );
			else
				strncpy( response, "accepted"NEWLINE, respLen ); //response[0] = '\0';
			break;
		
		case C11N_INT64:
		case C11N_UINT64:
		case C11N_DOUBLE:
		case C11N_STRUCT:
			break;
	}
	
	return 0;
}


/** 
 * Set the Parameter with Value
 */
cli_COMMAND_FUNCTION( cli_param )
{
	size_t i,j, len, c;
	
	debugf3("cli_param()"NEWLINE);
	
	if( argc == 1 ){
		strlcpy( response, "Usage: param <paramName> [value]"NEWLINE "Avaliable params are:\n", respLen);		//response[0] = '\0';
		// Выдать список параметров 
		///\todo пройтись по модулям
		len = strlen(response);
		for( i=0; i < MODULES_PARAMETERS_COUNT; ++i ){
			c = MODULES_PARAMETERS[i]->count;
			debugf( "%d\n", c );
			for( j=0; j < c; ++j ){
				//len += snprintf( response+len, respLen-len, "%s.%s\t %s"NEWLINE, PARAMETERS[j].name, PARAMETERS[j].description);
				len += snprintf( response+len, respLen-len, "%s.%s\t %s"NEWLINE, 
						MODULES_PARAMETERS[i]->name, 
						MODULES_PARAMETERS[i]->parameters[j].name, MODULES_PARAMETERS[i]->parameters[j].description);
			}
		}
	} else if( argc==2 ){
		cli_get( response, respLen, argc, argv, opts );
	} else if( argc==3 ){
		cli_set( response, respLen, argc, argv, opts );
	} else {
		strlcpy( response, "Usage: param <paramName> [value]"NEWLINE, respLen);
	}
	
	return 0;
}


/**
 * Print current date
 */
cli_COMMAND_FUNCTION( cli_time )
{
	char buffer[32];
	//time_t seconds1900;
	//struct ptptime_t ptptime;
	struct tm tm;
	time_t t;
	
	switch( argc ){
		case 1:
			t = time(0);
			localtime_r( &t, &tm );
			strftime( buffer, sizeofarr(buffer), "%Y.%m.%d %H:%M:%S"NEWLINE, &tm );
			//strftime(buffer, sizeof(buffer), "%a %b %d %H:%M:%S UTC %Y"NEWLINE, localtime(&seconds1900));
			break;
		case 2:
			if( 6 != sscanf( argv[1], "%4d.%2d.%2d %2d:%2d:%2d", &tm.tm_year, &tm.tm_mon, &tm.tm_mday, &tm.tm_hour, &tm.tm_min, &tm.tm_sec  ) ){
				strlcpy( response, "Wrong format. Usage: date [YEAR.MM.DD HH:MM:SS]"NEWLINE, respLen);
				return 0;
			}
			strftime( buffer, sizeofarr(buffer), "%Y.%m.%d %H:%M:%S"NEWLINE, &tm );
			debugf("cli_date(): %s", buffer );
			drv_rtc_SetByStructTM( &tm );
			break;
		default:
			strlcpy( response, "Usage: date [YEAR.MM.DD HH:MM:SS]"NEWLINE, respLen);
			return 0;
	}
	
	/// Prepare response.
	strlcpy( response, buffer, respLen);
	
	return 0;
}


/**
 * Print current date
 */
cli_COMMAND_FUNCTION( cli_date )
{
	char buffer[32];
	//time_t seconds1900;
	//struct ptptime_t ptptime;
	struct tm tm;
	time_t t;
	
	switch( argc ){
		case 1:
			t = drv_rtc_get(false);
			localtime_r( &t, &tm );
			strftime( buffer, sizeofarr(buffer), "%Y.%m.%d %H:%M:%S"NEWLINE, &tm );
			//strftime(buffer, sizeof(buffer), "%a %b %d %H:%M:%S UTC %Y"NEWLINE, localtime(&seconds1900));
			break;
		case 2:
			if( 6 != sscanf( argv[1], "%4d.%2d.%2d %2d:%2d:%2d", &tm.tm_year, &tm.tm_mon, &tm.tm_mday, &tm.tm_hour, &tm.tm_min, &tm.tm_sec  ) ){
				strlcpy( response, "Wrong format. Usage: date [YEAR.MM.DD HH:MM:SS]"NEWLINE, respLen);
				return 0;
			}
			strftime( buffer, sizeofarr(buffer), "%Y.%m.%d %H:%M:%S"NEWLINE, &tm );
			debugf("cli_date(): %s", buffer );
			drv_rtc_SetByStructTM( &tm );
			break;
		default:
			strlcpy( response, "Usage: date [YEAR.MM.DD HH:MM:SS]"NEWLINE, respLen);
			return 0;
	}
	
	/// Prepare response.
	strlcpy( response, buffer, respLen);
	
	return 0;
}


/**
 * Print current date
 */
cli_COMMAND_FUNCTION( cli_dateptp )
{
	char buffer[32];
	time_t seconds1900;
	struct ptptime_t ptptime;

	// Get the ethernet time values.
	ETH_PTPTime_GetTime(&ptptime);

	// Get the seconds since 1900.
	seconds1900 = (time_t) ptptime.tv_sec;

	// Format into a string.
	strftime(buffer, sizeof(buffer), "%a %b %d %H:%M:%S UTC %Y\n", localtime(&seconds1900));

	// Print the string.
	strlcpy( response, buffer, respLen);

	return 0;
}


cli_COMMAND_FUNCTION( cli_ptpd2 )
{
	char sign;
	const char *s;
	unsigned char *uuid;
	extern PtpClock ptpClock;
	size_t len=0;
	
	uuid = (unsigned char*) ptpClock.parentDS.parentPortIdentity.clockIdentity;

	/* Master clock UUID */
	len += snprintf( response+len, respLen-len, "master id: %02x%02x%02x%02x%02x%02x%02x%02x\n",
					uuid[0], uuid[1],
					uuid[2], uuid[3],
					uuid[4], uuid[5],
					uuid[6], uuid[7]);

	switch (ptpClock.portDS.portState)
	{
		case PTP_INITIALIZING:  s = "init";  		break;
		case PTP_FAULTY:        s = "faulty";   	break;
		case PTP_LISTENING:     s = "listening";  	break;
		case PTP_PASSIVE:       s = "passive";  	break;
		case PTP_UNCALIBRATED:  s = "uncalibrated"; break;
		case PTP_SLAVE:         s = "slave";   		break;
		case PTP_PRE_MASTER:    s = "pre master";  	break;
		case PTP_MASTER:        s = "master";   	break;
		case PTP_DISABLED:      s = "disabled"; 	break;
		default:                s = "?";			break;
	}

	/* State of the PTP */
	len += snprintf( response+len, respLen-len, "state: %s\n", s);

	/* One way delay */
	switch (ptpClock.portDS.delayMechanism)
	{
		case E2E:
			//telnet_puts("mode: end to end\n");
			//telnet_printf("path delay: %d nsec\n", ptpClock.currentDS.meanPathDelay.nanoseconds);
			len += snprintf( response+len, respLen-len, "mode: end to end\n");
			len += snprintf( response+len, respLen-len, "path delay: %d nsec\n", ptpClock.currentDS.meanPathDelay.nanoseconds);
			break;
		case P2P:
			//telnet_puts("mode: peer to peer\n");
			//telnet_printf("path delay: %d nsec\n", ptpClock.portDS.peerMeanPathDelay.nanoseconds);
			len += snprintf( response+len, respLen-len, "mode: peer to peer\n");
			len += snprintf( response+len, respLen-len, "path delay: %d nsec\n", ptpClock.portDS.peerMeanPathDelay.nanoseconds);
			break;
		default:
			//telnet_puts("mode: unknown\n");
			//telnet_printf("path delay: unknown\n");
			len += snprintf( response+len, respLen-len, "mode: unknown\n");
			len += snprintf( response+len, respLen-len, "path delay: unknown\n");
			/* none */
			break;
	}

	/* Offset from master */
	if (ptpClock.currentDS.offsetFromMaster.seconds) {
		len += snprintf( response+len, respLen-len, "offset: %d sec\n", ptpClock.currentDS.offsetFromMaster.seconds);
	} else {
		len += snprintf( response+len, respLen-len, "offset: %d nsec\n", ptpClock.currentDS.offsetFromMaster.nanoseconds);
	}

	/* Observed drift from master */
	sign = ' ';
	if (ptpClock.observedDrift > 0) sign = '+';
	if (ptpClock.observedDrift < 0) sign = '-';

	len += snprintf( response+len, respLen-len, "drift: %c%d.%03d ppm\n", sign, abs(ptpClock.observedDrift / 1000), abs(ptpClock.observedDrift % 1000));

	return 0;
}


/** 
 * Compilies help message.
 */
cli_COMMAND_FUNCTION( cli_help )
{
	size_t i, len;
	
	/// Clear string
	response[0] = '\0';
	len = 0;
	
	/// Loop over each shell command.
	for (i = 0; i < COMMANDS_COUNT; ++i) {
		len += snprintf( response+len, respLen-len, "%s\t %s"NEWLINE, COMMANDS[i].name, COMMANDS[i].help);
	}
	
	return 0;
}


/** 
 * This function returns 1. This must be recognized as signal 
 * to close current TCP/Telnet connection.
 * Its analog shell_exit uses FALSE.
 * @return 1
 */
cli_COMMAND_FUNCTION( cli_exit )
{
	strlcpy( response, "Closing connection..."NEWLINE, respLen);
	return 1;
}


/** 
 * Information about device.
 */
cli_COMMAND_FUNCTION( cli_info )
{
	char ipbuf[16];
	extern struct netif xnetif;
	
	snprintf( response, respLen,
			"Hello, %s."								NEWLINE 
			"UID32: 0x%X; UID8: 0d%d;"					NEWLINE
			"MAC: %02X:%02X:%02X:%02X:%02X:%02X, IP: %s"NEWLINE
			"Hostname: %s"								NEWLINE, 
			
			ipaddr_ntoa_r( &((SenderOptions_t*)opts)->destinationIP, ipbuf, sizeof(ipbuf)),
			uid32(), uid8(), 
			xnetif.hwaddr[0], xnetif.hwaddr[1], xnetif.hwaddr[2], xnetif.hwaddr[3], xnetif.hwaddr[4], xnetif.hwaddr[5], 
			ipaddr_ntoa_r(&xnetif.ip_addr, ipbuf, sizeof(ipbuf)),
			xnetif.hostname );
	return 0;
}


/** 
 * Вывести список задач FreeRTOS
 */
cli_COMMAND_FUNCTION( cli_tasklist )
{
	//char buffer[10];
	
	///It is recommended that production systems call uxTaskGetSystemState() directly
	//vTaskList( buffer /*+ strlen(pcHeader)*/);
	
	strlcpy( response, "cli_tasklist: Not implemented yet.", respLen);
	return 0;
}


/** 
 * Clear screen. Useful with telnet, but not with netcat.
 */
cli_COMMAND_FUNCTION( cli_clear )
{
	//*response = '\f';
	strlcpy( response, "\x1b[2J\x1b[H", respLen);
	return 0;
}


/** 
 * Reset uC.
 */
cli_COMMAND_FUNCTION (cli_reset) //( char *response, size_t respLen, int argc, /*const*/ char *argv[] ) 
{
	NVIC_SystemReset();
	// We will never come here
	return 0;
}


/** 
 * Unknown command.
 */
//int8_t cli_unknown( char *response, size_t respLen, int argc, /*const*/ char *argv[] )
//{
//	snprintf( response, respLen, "Unknown command \"%s\". Type \"help\" or \"?\" to see valid commands."NEWLINE, argv[0] );
//	return 0;
//}
