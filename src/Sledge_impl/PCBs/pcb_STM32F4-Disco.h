/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php
 *      http://opensource.org/licenses/mit-license.php
 */
/**
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb@ya.ru>
 * \brief 	This file defines pin lists and their appointments.
 * 			Constatants, pointers, arrays and all other C features 
 * 			are implemented in bsp_pcb.c
 * STM32-Discovery
 */


#ifndef __pcb_STM32F4_Disco_H
#define __pcb_STM32F4_Disco_H 100


/** ┌———————————————————————————————————————————————————————————————————————┐
	│						Main start up parameters						│  
	└———————————————————————————————————————————————————————————————————————┘ */
#define HSE_Crystal_MHz  8
#define PLL_M 	HSE_Crystal_MHz


/** ┌———————————————————————————————————————————————————————————————————————┐
	│						Outouts & LEDs control							│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** List of regular LEDs */
#define _LEDS_  	PD12, PD13, PD14, PD15,

#define LED_Green 	PD12
#define LED_Orange 	PD13
#define LED_Red 	PD14
#define LED_Blue 	PD15

/** List of LEDs with controllable brightness. 
 * 	@note Must be connected to TIMx channels. Look at alt. functions of GPIOs
 */
#define _LEDS_PWM_  			PD12, PD13, PD14, PD15,
#define LEDS_PWM_TIM_HANDLER	TIMS[4]
#define LEDS_PWM_TIM_FREQ		500000
#define LEDS_PWM_TIM_PERIOD 	( (SystemCoreClock / LEDS_PWM_TIM_FREQ) - 1 )

/** List of RGB LEDs with controllable brightness. 
 * 	@note Must be connected to TIMx channels. Look at alt. functions of GPIOs
 */
//#define _LEDS_RGB_ 	{ 	/*Red │Green│ Blue│ common */ \
							{ PH11, PH12, PH10,      1 }, \
						}

/** ┌———————————————————————————————————————————————————————————————————————┐
	│					Segment indicators GPIOs							│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** Список пинов, управляющих общими анодами(катодами). Определяет количество индикаторов
 *  @note если работаем с 5V внимательно смотреть в даташит на предмет совместимости ноги с 5В.
 */
//#define _CommonGpios_ 	{ PG2, PG3, PG4, PG5, PG6, }

/** Список ножек, управляющих сегментами. Определяет количество сегментов
 * 	7 в обычном, 8 с точкой, 14 с Ж внутри, 16 + по 2 сегмента сверху и снизу
 *  @note если работаем с 5V внимательно смотреть в даташит на предмет совместимости ноги с 5В.
 */
//#define _SegmentGpios_  { PE7, PE8, PE9, PE10, PE11, PE12, PE13, PE14, }


/** ┌———————————————————————————————————————————————————————————————————————┐
	│				Inputs, Buttons and joystick control					│  
	└———————————————————————————————————————————————————————————————————————┘ */
/** List of buttons */
#define pcbBUTTONS 	{ PA0, }
#define ButtonWAKEUP  PA0
#define BUTTON_Bootloader 	PA0


/** ┌———————————————————————————————————————————————————————————————————————┐
	│							COM ports									│  
	└———————————————————————————————————————————————————————————————————————┘ */
/// \Todo Make only USART, TX, RX, TX_DMA_Stream, RX_DMA_Stream definable, others automaticly.
//#define _COMs_	{  /* USART │          RCC         │     IRQn   │ TX │  RX │     GPIO_AF    │      DMA      */  \
						{ USART2, RCC_APB1Periph_USART2, USART2_IRQn, PA2,  PA3,  GPIO_AF_USART2, USART2_DMA_BSP }, \
						{ USART2, RCC_APB1Periph_USART2, USART2_IRQn, PD5,  PD6,  GPIO_AF_USART2, USART2_DMA_BSP }, \
						{ USART3, RCC_APB1Periph_USART3, USART3_IRQn, PC11, PC10, GPIO_AF_USART3, USART3_DMA_BSP }, \
						{ USART1, RCC_APB2Periph_USART1, USART1_IRQn, PB6,  PB7,  GPIO_AF_USART1, USART1_DMA_BSP }, \
					}
#define _COM_debug_ 	USART2, RCC_APB1Periph_USART2, USART2_IRQn, PD5,  PD6,  GPIO_AF_USART2, USART2_DMA_BSP
//{ USART1, RCC_APB2Periph_USART1, USART1_IRQn, PB6,  PB7,  GPIO_AF_USART1, USART1_DMA_BSP }


/** ┌———————————————————————————————————————————————————————————————————————┐
	│							Ethernet									│  
	└———————————————————————————————————————————————————————————————————————┘ */
#define ETH_LINK_GPIO 	PB14
#define Eth_Link_IRQ_handler  EXTI14_IRQHandler

#define ETH_MDIO                            PA2
#define ETH_MDC                             PC1
#define ETH_PPS_OUT                         PB5

#define ETH_MII_CRS                         PH2
#define ETH_MII_COL                         PH3
#define ETH_MII_RX_ER                       PI10  // posible conflict with LED4 on EVK407I
#define ETH_MII_RX_CLK                      PA1
#define ETH_MII_RX_DV                       PA7
#define ETH_MII_RXD0                        PC4
#define ETH_MII_RXD1                        PC5
#define ETH_MII_RXD2                        PH6
#define ETH_MII_RXD3                        PH7
#define ETH_MII_TX_EN                       PB11
#define ETH_MII_TX_CLK                      PC3
#define ETH_MII_TXD0                        PB12
#define ETH_MII_TXD1                        PB13
#define ETH_MII_TXD2                        PC2
#define ETH_MII_TXD3                        PB8

#define ETH_RMII_REF_CLK                    PA1
#define ETH_RMII_CRS_DV                     PA7
#define ETH_RMII_RXD0                       PC4
#define ETH_RMII_RXD1                       PC5
#define ETH_RMII_TX_EN                      PB11 //or can be PG11
#define ETH_RMII_TXD0                       PB12 //or can be PG13
#define ETH_RMII_TXD1                       PB13 //or can be PG14



#endif /* __pcb_STM32F4_Disco_H */
