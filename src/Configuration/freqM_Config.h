/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php  
 *      http://opensource.org/licenses/mit-license.php  
 */
/** 
 * @author  Kyb Sledgehammer <i.kyb[2]ya,ru>
 * @brief   This is the configuration file for Frequency Measurement Module (FMM) for IF5646 sensor
 *          Здесь находятся константы, параметры и ограничения времени компиляции.
 */

#ifndef __freqM_Config_H
#define __freqM_Config_H


// Включить режим отладки модуля. Нужно при принизкоуровневой работе с переферией, и поиске проблем.
//#define FREQM_DEBUG 1


/// Привязать функцию-обработчик прерывания по переполнению опорного таймера к действительному вектору прерывания.
///\note Как альтернатива можно вызывать эту фунцию из обработчика прерывания.
#define freqM_RefTimer_Upd_IRQHandler     TIM1_UP_IRQHandler
/// Определить функцию-обработчик прерывания по захвату/сравнению канала 1 опорного таймера
#define freqM_RefTimer_CC_IRQHandler      TIM1_CC_IRQHandler
/// Обработчик прерывания переполнения таймера, задающего дискретизацию
#define freqM_SampleRateTimer_IRQHandler  TIM7_IRQHandler



#endif //__freqM_Config_H
