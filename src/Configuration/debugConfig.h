/**
 * @file    debugConfig.h
 * @author  Kyb <i,Kyb[2]ya,ru>
 * @version 1.0.0
 * @date    2015-mar-19
 * @brief   
 */
  
#ifndef __DEBUG_CONFIG_H
#define __DEBUG_CONFIG_H	100


/// Don't forget bit-field
#define MAIN_DEBUG_LEVEL  7 	



#endif /* __DEBUG_CONFIG_H */  
