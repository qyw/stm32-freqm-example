/**
 * @file 	mainConfig.h
 * @author	Kyb
 * @version	1.2.5
 * @date	2015-Apr-21
 * @brief   This file contains main definitions for the program.
 *			
 */

/** Define to prevent recursive inclusion -----------------------------------*/
#ifndef __MAIN_CONFIG_H
#define __MAIN_CONFIG_H


/// 0: No RTOS, 1: FreeRTOS, others: ToDo
#define USE_RTOS 	0

/// New line is: 0=LF (Linux), 1=CR (old Mac), 2=CR+LF (Windows)
#define NEWLINE_OPTION 	0


#define USE_USB_VIRTUAL_COM_PORT	1
/** Uncomment SERIAL_DEBUG to enables retarget of printf to  serial port (COM1 on STM32 evalboard) 
  * for debug purpose */
#define SERIAL_DEBUG				1


/**
 * Назначение светодиодов
 * На каком пине какого цвета светодиод определено в файле bsp_pcb.h
 * тип GPIO_t*
 */
#include "Sledge_impl/bsp_pcb.h"
//#include "task_prioriries.h"


//#define LedBACKGROUND	LED_White
#define LedFAULT  		LED_Red

// To fill struct tm on start up if RTC was reset
#define DEFAULT_TIME  	/*sec*/0, /*min*/0, /*hour*/0, \
						/*monthday*/11, /*month*/8, /*year*/2015-1900, \
						/*weakday*/0, /*yearday*/0, /*isdst*/0



#endif /* __MAIN_CONFIG_H */
