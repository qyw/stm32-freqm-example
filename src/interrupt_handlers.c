/**
 * Copyright (c) 2016 [Ivan Kuvaldin](mailto:i.kyb@ya.ru)
 * [The MIT License](https://opensource.org/licenses/MIT)
 * \brief   Main Interrupt Service Routines.
 *          This file provides template for all exceptions handler and 
 *          peripherals interrupt service routine.
 */


//#include "mainConfig.h"

///\addtogroup "Sledge includes" \{
//#include "Sledge/dassel/logger/comport.h"
//#include "Sledge/bsp.h"
#include "Sledge/drv/stm32/sledge_stm32_RNG.h"
#include "Sledge/drv/stm32/sledge_stm32_gpio.h"
#include "Sledge/drv/stm32/sledge_stm32_timers.h"

#include "Sledge/assert.h"

///\}


/**
 * Функции ниже нужны чтоб разветвить программу из обединенного вектора прерывания.
 * Пользователь может не мучаться, когда 2 переферийных модуля ссылают прерывания в одну фунуцию.
 * \note Однако следует помнить, что контроллер прерываний NVIC всё равно задает приоритеты на группу.
 */
void __attribute__((weak)) TIM1_UP_IRQHandler(void) {};
void __attribute__((weak)) TIM8_UP_IRQHandler(void) {};
void __attribute__((weak)) TIM10_IRQHandler(void) {};
void __attribute__((weak)) TIM13_IRQHandler(void) {};
void __attribute__((weak)) RNG_IRQHandler(void) {};
void __attribute__((weak)) HASH_IRQHandler(void) {};

void __attribute__((weak)) EXTI5_IRQHandler(void) {};
void __attribute__((weak)) EXTI6_IRQHandler(void) {};
void __attribute__((weak)) EXTI7_IRQHandler(void) {};
void __attribute__((weak)) EXTI8_IRQHandler(void) {};
void __attribute__((weak)) EXTI9_IRQHandler(void) {};
void __attribute__((weak)) EXTI10_IRQHandler(void) {};
void __attribute__((weak)) EXTI11_IRQHandler(void) {};
void __attribute__((weak)) EXTI12_IRQHandler(void) {};
void __attribute__((weak)) EXTI13_IRQHandler(void) {};
void __attribute__((weak)) EXTI14_IRQHandler(void) {};
void __attribute__((weak)) EXTI15_IRQHandler(void) {};



void HASH_RNG_IRQHandler(void) 
{
	RNG_IRQHandler();
	HASH_IRQHandler();
}


/** @brief  This function handles TIM1 update interrupt request and TIM10 global.
  * @param  None
  * @retval None
  */
void TIM1_UP_TIM10_IRQHandler(void) 
{
	if( drv_TIM_GetITStatus_( &TIMS[1], tim_interrupt_UPDATE ) ){
		if(TIM1_UP_IRQHandler) TIM1_UP_IRQHandler();
	}else if( drv_TIM_GetITStatus_( &TIMS[10], tim_interrupt_UPDATE ) ){
		if(TIM10_IRQHandler) TIM10_IRQHandler();
	}
}


void TIM8_UP_TIM13_IRQHandler()
{
	if( drv_TIM_GetITStatus_( &TIMS[8], tim_interrupt_UPDATE ) ){
		if(TIM8_UP_IRQHandler) TIM8_UP_IRQHandler();
	}else if( drv_TIM_GetITStatus_( &TIMS[13], tim_interrupt_UPDATE ) ){
		if(TIM13_IRQHandler) TIM13_IRQHandler();
	}
	//extern void HwTim_UpdHandler(void);
	//if( TIM_GetITStatus( sst_HwTim
}



/******************************************************************************/
/*            Cortex-M4 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
 * @brief	This function handles SysTick Handler. Calls FreeRTOS systick handler.
 *	@note	If systick is not used for other purposes the better solution is 
 * 				#define xPortSysTickHandler	SysTick_Handler
 *			in FreeRTOSConfig.h
 */
/*void SysTick_Handler(void)
{
	extern void xPortSysTickHandler(void);
	xPortSysTickHandler(); 
}*/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
	abort_msg(__func__);
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
	abort_msg(__func__);
	// Go to infinite loop when Memory Manage exception occurs
	while (1) {}
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
	abort_msg(__func__);
	// Go to infinite loop when Memory Manage exception occurs
	while (1) {}
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
	abort_msg(__func__);
	// Go to infinite loop when Memory Manage exception occurs
	while (1) {}
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
	abort_msg(__func__); //assert_failed(__FILE__, __LINE__);
	// Go to infinite loop when Memory Manage exception occurs
	while(1) {}
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
	abort_msg(__func__);
}

/**
 * @brief  This function handles FPU exception.
 */
void FPU_IRQHandler(void)
{
	abort_msg(__func__);
}




/******************************************************************************/
/*                 STM32F4xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f4xx.s).                                               */
/******************************************************************************/
/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

