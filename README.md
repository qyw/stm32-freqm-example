# Example project for [STM32-FreqM](https://bitbucket.org/qyw/stm32-freqm)

```sh
git clone --recursive git@bitbucket.org:qyw/stm32-freqm-example.git
```


## Подключение, соединение
Соедините **PA8** и **PE0**. Подайте на них входной сигнал.  
Подключите UART **PD5** - Tx контроллера, Rx - компьютера. **PD6** - Rx контроллера, в данном случае не актуально. Можно без UART, через SWO (программатор).
Ну забудьте про общую шину **GND**.


## Компиляция, прошивка
В папке `.keil` находится файл проекта `.uvprojx`. Откройте в Keil v5. `F7` - компиляция, `F8` - прошить, `ctrl+F5` - прошивка и отладка.


## Запуск, проверка
_Зелёный_ светодиод мигает приблизительно полтора раза в секукнду - это контроллер говорит "я живой".  
_Красный_ светится, если нет сигнала, включается с задержкой в несколько минут.
_Синий_ светодиод переключается по событию *частота посчитана*. Если нет проверьте сигнал.  
В окошке UART можно увидеть текущую частоту сигнала.  

Также можно прочитать частоту через SWO. Есть 2 способа: 

- Через Keil. Запустить контроллер в режиме отладки `Ctrl+F5` или меню *Debug->Start/Stop...*. Далее меню *View->Serial Windows->Debug(Printf) viewer*  
- Через ST-Link. Кнопка *SWV* на панели или меню *ST-Link->Printf via SWO Viewer*


## Обновление
```
git pull
git submodule update
```

### Обновление подмодулей отдельно от основного репозитория
```sh
git submodule update --remote
```
После чего желательно сделать коммит
```sh
git add lib/*
git commit -m 'updated submodules'
```


## Authors and License
Copyright (c) 2016, [Ivan Kuvaldin](mailto:i.kyb@ya.ru). All rights reserved.  
[The MIT License](https://opensource.org/licenses/MIT)